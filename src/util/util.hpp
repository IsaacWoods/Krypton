#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <thread>
#include <string.h>
#include <util/LIB_stb_image.h>
#include <util/maths.hpp>

enum RendererType
{
  BASIC,

  NONE
};

struct KryptonOptions
{
  KryptonOptions() :
    width(800),
    height(600),
    windowTitle("Krypton Renderer"),

    rendererType(BASIC),
    desiredFramerate(60),
    maxBatchingSize(500),
    shaderName("basicShader"),

    profileFrequency(1.0d),
    shouldProfile(false)
  { }

  // --- Window ---
  unsigned int width, height;
  std::string windowTitle;

  // --- Rendering ---
  RendererType rendererType;
  unsigned int desiredFramerate;
  unsigned int maxBatchingSize;
  std::string shaderName;

  // --- Profiling ---
  double profileFrequency;
  bool shouldProfile;
};

namespace Util
{
  static std::chrono::system_clock::time_point g_epoch = std::chrono::high_resolution_clock::now();

  inline void pauseConfirm()
  {
    std::cout << "--- PAUSE ---" << std::endl;
    std::cin.get();
  }

  inline void checkGLError()
  {
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)   std::cerr << "Encountered OpenGL error: " << error << std::endl;
  }

  template<typename T>
  inline T clamp(const T& value, const T& lowerClamp, const T& upperClamp)
  {
    if (value < lowerClamp)   return lowerClamp;
    if (value > upperClamp)   return upperClamp;
    else            return value;
  }

  inline std::vector<Vector2> constructUVMap(int textureSize, const Vector2& textureOffset, const Vector2& textureSampleSize)
  {
    std::vector<Vector2> result;

    result.push_back(Vector2(textureOffset.x() / (float) textureSize, (textureOffset.y() + textureSampleSize.y()) / (float) textureSize));                // [0, 1]
    result.push_back(Vector2(textureOffset.x() / (float) textureSize, textureOffset.y() / (float) textureSize));                            // [0, 0]
    result.push_back(Vector2((textureOffset.x() + textureSampleSize.x()) / (float) textureSize, textureOffset.y() / (float) textureSize));                // [1, 0]
    result.push_back(Vector2((textureOffset.x() + textureSampleSize.x()) / (float) textureSize, (textureOffset.y() + textureSampleSize.y()) / (float) textureSize));  // [1, 1]

    return result;
  }

  inline std::vector<Vector2> constructUVMapByOffset(int textureSize, const Vector2& textureOffset, const Vector2& textureSampleSize)
  {
    Vector2 textureOffsetPixels = textureOffset * textureSampleSize;
    return constructUVMap(textureSize, textureOffsetPixels, textureSampleSize);
  }

  inline double getTime()
  {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - g_epoch).count() / 1000000000.0d;
  }

  inline void sleep(int milliseconds)
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
  }

  inline std::string readFile(const std::string& filePath)
  {
    // if (access(filePath.c_str(), R_OK) == -1)
    // {
    //   std::cerr << "Could not locate file: " << filePath << std::endl;
    //   pauseConfirm();
    //
    //   return NULL;
    // }

    FILE* file = fopen(filePath.c_str(), "rt");
    fseek(file, 0, SEEK_END);
    unsigned int long length = ftell(file);

    char* data = new char[length + 1];
    memset(data, 0, length + 1);
    fseek(file, 0, SEEK_SET);
    fread(data, 1, length, file);

    std::string result(data);
    delete[] data;
    fclose(file);

    return result;
  }

  inline std::vector<std::string> split(const std::string& str, char deliminator)
  {
    std::vector<std::string> elems;

    const char* cstr = str.c_str();
    unsigned int length = (unsigned int) str.length();
    unsigned int start = 0, end = 0;

    while (end <= length)
    {
      while (end <= length)
      {
        if (cstr[end] == deliminator)
          break;

        end++;
      }

      elems.push_back(str.substr(start, end - start));
      start = end - 1;
      end = start;
    }

    return elems;
  }
}

#endif // UTIL_H_INCLUDED
