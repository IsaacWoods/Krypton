#ifndef WINDOW_H
#define WINDOW_H

#include <string>
#include <iostream>
#include <GLFW/glfw3.h>
#include <util/util.hpp>

class Window
{
  public:
    Window(const KryptonOptions& options);
    ~Window();

    bool shouldClose() const;
    void update();
    void destroy();

    static void errorCallback(int error, const char* description);
    static void framebufferSizeCallback(GLFWwindow* window, int width, int height);
  private:
    const KryptonOptions& m_options;
    GLFWwindow* m_window;
};

#endif // WINDOW_H
