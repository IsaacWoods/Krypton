#include <game.hpp>

class TestGame : public Game
{
  public:
    TestGame(const KryptonOptions& options);

    void init();
    void update(float delta);
    void render(Renderer& renderer);
  private:
    TiledScene m_scene;
    Texture m_cornerTex, m_floorTex, m_wallTex;
};

TestGame::TestGame(const KryptonOptions& options) :
  Game(options),
  m_scene(10, 10, 16),  // Tile size apparently doesn't work?!
  m_cornerTex("corner.png"),
  m_floorTex("floor.png"),
  m_wallTex("wall.png")
{ }

void TestGame::init()
{
  std::vector<Vector2> defaultUV { Vector2(0, 1), Vector2(0, 0), Vector2(1, 0), Vector2(1, 1) };

  m_scene.addTileDefinition(new TileDefinition(0, &m_wallTex, defaultUV));    // Left Vertical Wall
  m_scene.addTileDefinition(new TileDefinition(1, &m_wallTex, defaultUV));    // Right Vertical Wall
  m_scene.addTileDefinition(new TileDefinition(2, &m_wallTex, defaultUV));    // Top Horizontal Wall
  m_scene.addTileDefinition(new TileDefinition(3, &m_wallTex, defaultUV));    // Bottom Horizonal Wall
  m_scene.addTileDefinition(new TileDefinition(4, &m_cornerTex, defaultUV));    // Botom-Right Corner
  m_scene.addTileDefinition(new TileDefinition(5, &m_cornerTex, defaultUV));    // Bottom-Left Corner
  m_scene.addTileDefinition(new TileDefinition(6, &m_cornerTex, defaultUV));    // Top-Right Corner
  m_scene.addTileDefinition(new TileDefinition(7, &m_cornerTex, defaultUV));    // Top-Left Corner
  m_scene.addTileDefinition(new TileDefinition(8, &m_floorTex, defaultUV));    // Floor

  // Walls
  m_scene.setTile(2, 2, 6);
  m_scene.setTile(2, 3, 1);
  m_scene.setTile(2, 4, 1);
  m_scene.setTile(2, 5, 1);
  m_scene.setTile(2, 6, 4);
  m_scene.setTile(3, 6, 3);
  m_scene.setTile(4, 6, 3);
  m_scene.setTile(5, 6, 3);
  m_scene.setTile(6, 6, 3);
  m_scene.setTile(7, 6, 5);
  m_scene.setTile(7, 5, 0);
  m_scene.setTile(7, 4, 0);
  m_scene.setTile(7, 3, 0);
  m_scene.setTile(7, 2, 7);
  m_scene.setTile(6, 2, 2);
  m_scene.setTile(5, 2, 2);
  m_scene.setTile(4, 2, 2);
  m_scene.setTile(3, 2, 2);

  // Floor
  m_scene.setTile(3, 3, 8);
  m_scene.setTile(3, 4, 8);
  m_scene.setTile(3, 5, 8);
  m_scene.setTile(4, 3, 8);
  m_scene.setTile(4, 4, 8);
  m_scene.setTile(4, 5, 8);
  m_scene.setTile(5, 3, 8);
  m_scene.setTile(5, 4, 8);
  m_scene.setTile(5, 5, 8);
  m_scene.setTile(6, 3, 8);
  m_scene.setTile(6, 4, 8);
  m_scene.setTile(6, 5, 8);
}

void TestGame::update(float delta)
{
  m_scene.update(delta);
}

void TestGame::render(Renderer& renderer)
{
  m_scene.render(renderer);
}

// int main()
// {
//   KryptonOptions options;
//   options.width = 1000;
//   options.height = 800;
//
//   TestGame game(options);
//   game.start();
//
//   return 0;
// }
