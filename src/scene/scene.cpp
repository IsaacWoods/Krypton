#include <scene/scene.hpp>
#include <rendering/renderer.hpp>

Scene::~Scene()
{
  for (auto* object : m_objects)
    delete object;
}

Scene& Scene::addObject(GameObject* object)
{
  m_objects.push_back(object);
  return *this;
}

void Scene::update(float delta)
{
  for (auto* object : m_objects)
    object->update(delta);
}

void Scene::render(Renderer& renderer) const
{
  renderer.prepareForScene();
  renderer.startBatch(m_projectionMatrix, m_scale);

  for (auto* object : m_objects)
    object->render(renderer);

  renderer.flushBatch();
}

void Scene::setSize(int width, int height)
{
  m_width = width;
  m_height = height;
  updateProjectionMatrix();
}

void Scene::updateProjectionMatrix()
{
  m_projectionMatrix = Matrix4::orthographicProj(0, m_width, 0, m_height, 0.0f, 100.0f);
}
