#include <scene/gameobject.hpp>

GameObject::GameObject(const Vector2& position, float rotation) :
  m_position(position),
  m_rotation(rotation)
{ }

GameObject::~GameObject()
{
  for (auto* component : m_components)
    delete component;
}

GameObject& GameObject::addComponent(Component* component)
{
  m_components.push_back(component);
  component->setParent(this);
  
  return *this;
}

void GameObject::update(float delta)
{
  for (auto* component : m_components)
    component->update(delta);
}

void GameObject::render(Renderer& renderer) const
{
  for (auto* component : m_components)
    component->render(renderer);
}
