### 14/06/2015 - Why The Project Sucks (Currently)
Krypton sucks. It was meant to be a side-project, that was fast, easy to develop and simple. Currently, it's structure
is like it's meant to be a serious project. I'm not saying I want to write bad code, but I do want to simplify
the code base. Krypton is meant to be easy to hack stuff on to, mess around with new rendering pipelines, but also
to provide some cool features. Stuff like the Renderer framework is probably good code structure if Krypton was a
project with any design motive behind it at all, but with a constantly evolving project, it's actually just a pain in
the ass. We need yet another massive overhaul!

* Remove the pain-in-the-ass Renderer framework
* Put some of the boring stuff into a `/util/` folder and change includes
* Get rid of the `Krypton` class and move it's responsibilities into `Game`

Problem: we need a way to store data like scenes that we need to access from multiple methods.
With a common `Game` class, how are we meant to do this?
