#include <util/texture.hpp>
#include <iostream>
#include <assert.h>
#include <util/util.hpp>

Texture::Texture(const std::string& name, GLenum target, GLfloat filter, GLenum internalFormat, GLenum format, bool clamp, GLenum attachment) :
  m_target(target)
{
  std::string filename = "assets/textures/" + name;

  int bytesPerPixel;
  unsigned char* data = stbi_load(filename.c_str(), &m_width, &m_height, &bytesPerPixel, 4);

  if (data == NULL)
  {
    std::cerr << "Could not load texture file: " << filename << std::endl;
    Util::pauseConfirm();
  }

  generateTexture(data, filter, internalFormat, format, clamp);
  generateRenderTarget(attachment);
  stbi_image_free(data);
}

Texture::Texture(unsigned int width, unsigned int height, unsigned char* data, GLenum target, GLfloat filter, GLenum internalFormat, GLenum format, bool clamp, GLenum attachment) :
  m_width(width),
  m_height(height),
  m_target(target)
{
  generateTexture(data, filter, internalFormat, format, clamp);
  generateRenderTarget(attachment);
}

Texture::~Texture()
{
  glDeleteTextures(1, &m_handle);
  if (m_frameBuffer) glDeleteFramebuffers(1, &m_frameBuffer);
  if (m_renderBuffer) glDeleteRenderbuffers(1, &m_renderBuffer);
}

void Texture::bind(unsigned int texSlot) const
{
  assert(texSlot >= 0 && texSlot <= 31);

  glActiveTexture(GL_TEXTURE0 + texSlot);
  glBindTexture(m_target, m_handle);
}

void Texture::bindAsRenderTarget() const
{
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer);
  glViewport(0, 0, m_width, m_height);
}

void Texture::operator=(Texture texture)
{
  char* temp[sizeof(Texture) / sizeof(char)];
  memcpy(temp, this, sizeof(Texture));
  memcpy(this, &texture, sizeof(Texture));
  memcpy(&texture, temp, sizeof(Texture));
}

void Texture::generateTexture(unsigned char* data, GLfloat filter, GLenum internalFormat, GLenum format, bool clamp)
{
  glGenTextures(1, &m_handle);
  glBindTexture(m_target, m_handle);

  glTexParameterf(m_target, GL_TEXTURE_MIN_FILTER, filter);
  glTexParameterf(m_target, GL_TEXTURE_MAG_FILTER, filter);

  if (clamp)
  {
    glTexParameteri(m_target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(m_target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  }

  glTexImage2D(m_target, 0, internalFormat, m_width, m_height, 0, format, GL_UNSIGNED_BYTE, data);

  if (  filter == GL_NEAREST_MIPMAP_NEAREST ||
      filter == GL_NEAREST_MIPMAP_LINEAR ||
      filter == GL_LINEAR_MIPMAP_NEAREST ||
      filter == GL_LINEAR_MIPMAP_LINEAR)
    {
      glGenerateMipmap(m_target);
      GLfloat maxAnisotropy;
      glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);
      glTexParameterf(m_target, GL_TEXTURE_MAX_ANISOTROPY_EXT, Util::clamp(maxAnisotropy, 0.0f, 8.0f));
    }
    else
    {
      glTexParameterf(m_target, GL_TEXTURE_BASE_LEVEL, 0);
      glTexParameterf(m_target, GL_TEXTURE_MAX_LEVEL, 0);
    }
}

void Texture::generateRenderTarget(GLenum attachment)
{
  if (attachment == 0)
    return;

  GLenum drawBuffer;
  bool hasDepth = false;

  if (attachment == GL_DEPTH_ATTACHMENT)
  {
    drawBuffer = GL_NONE;
    hasDepth = true;
  }
  else
    drawBuffer = attachment;

  if (attachment == GL_NONE)
    return;

  if (m_frameBuffer == 0)
  {
    glGenFramebuffers(1, &m_frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer);
  }

  glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, m_target, m_handle, 0);

  if (m_frameBuffer == 0)
    return;

  if (!hasDepth)
  {
    glGenRenderbuffers(1, &m_renderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, m_renderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_ATTACHMENT, m_width, m_height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_renderBuffer);
  }

  glDrawBuffers(1, &drawBuffer);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
  {
    std::cerr << "Framebuffer creation failed!" << std::endl;
    Util::pauseConfirm();
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
