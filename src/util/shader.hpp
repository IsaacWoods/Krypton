#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <vector>
#include <map>
#include <GL/glew.h>
#include <util/maths.hpp>
#include <util/texture.hpp>

class Shader
{
  public:
    Shader(const std::string& name);
    ~Shader();

    void bind() const;
    void unbind() const;

    void streamUniform(const std::string& mapping, int value) const;
    void streamUniform(const std::string& mapping, float value) const;
    void streamUniform(const std::string& mapping, const Vector2& value) const;
    void streamUniform(const std::string& mapping, const Vector3& value) const;
    void streamUniform(const std::string& mapping, const Vector4& value) const;
    void streamUniform(const std::string& mapping, Matrix4 value) const;
    void streamUniform(const std::string& mapping, unsigned int samplerSlot, const Texture& texture) const;
  private:
    GLuint m_programHandle;
    std::map<std::string, GLuint> m_uniformMap;

    void generateShader(const std::string& vertexSource, const std::string& fragmentSource);
    void operator=(const Shader& other) { }
};

#endif // SHADER_H
