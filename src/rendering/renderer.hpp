#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <rendering/light.hpp>
#include <rendering/renderable.hpp>
#include <util/texture.hpp>
#include <util/shader.hpp>
#include <util/util.hpp>

#define CHECK_BATCH_OVERFLOW

class Renderer
{
  public:
    Renderer(const KryptonOptions& options);
    ~Renderer();

    void prepareForScene();
    void startBatch(const Matrix4& projectionMatrix, const Vector2& sceneScale);
    void submit(const Vector2& position, const Vector2& size);
    void submit(const Vector2& position, const Vector2& size, const std::vector<Vector2>& uvMap);
    void flushBatch();

    inline void addLight(const Light& light)         { m_lights.push_back(&light); }
    inline void addTexture(const Texture& texture)   { m_batchingDiffuseTexture = &texture; } // TODO
    inline void submit(const Renderable& renderable) { submit(renderable.getPosition(), renderable.getSize(), renderable.getUVMap()); }
  private:
    const KryptonOptions& m_options;
    bool m_isBatching;

    const Matrix4* m_batchingProjectionMatrix;
    const Texture* m_batchingDiffuseTexture; // TODO: get rid of this when we move to having a texture array
    const Vector2* m_batchingSceneScale;

    GLuint m_vertexArrayHandle;
    GLuint m_vertexBufferHandle;
    GLuint m_indexBufferHandle;
    Vertex* m_buffer;
    unsigned int m_indexCount;
    std::vector<const Light*> m_lights;
    Shader m_shader;

    #ifdef CHECK_BATCH_OVERFLOW
      unsigned int m_batchCounter;
    #endif
};

#endif // RENDERER_HPP
