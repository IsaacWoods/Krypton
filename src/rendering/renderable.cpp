#include <rendering/renderable.hpp>
#include <rendering/renderer.hpp>

Renderable::Renderable(const Vector2& size, Texture* texture) :
  Component(),
  m_size(size),
  m_texture(texture)
{
  m_UVMap.push_back(Vector2(0.0f, 1.0f));   // [0, 1]
  m_UVMap.push_back(Vector2(0.0f, 0.0f));   // [0, 0]
  m_UVMap.push_back(Vector2(1.0f, 0.0f));   // [1, 0]
  m_UVMap.push_back(Vector2(1.0f, 1.0f));   // [1, 1]
}

Renderable::Renderable(const Vector2& size, Texture* texture, const std::vector<Vector2>& uvMap) :
  Component(),
  m_size(size),
  m_texture(texture),
  m_UVMap(uvMap)
{ }

void Renderable::update(float delta)
{

}

void Renderable::render(Renderer& renderer) const
{
  renderer.addTexture(*m_texture);
  renderer.submit(*this);
}
