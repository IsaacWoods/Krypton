#ifndef RENDERABLE_H
#define RENDERABLE_H

#include <vector>
#include <util/maths.hpp>
#include <util/texture.hpp>
#include <scene/gameobject.hpp>

class Renderable : public Component
{
  public:
    Renderable(const Vector2& size, Texture* texture);
    Renderable(const Vector2& size, Texture* texture, const std::vector<Vector2>& uvMap);
    ~Renderable() { }

    const Vector2& getPosition()            const { return m_parent->getPosition();   }
    const Vector2& getSize()                const { return m_size;                    }
    const std::vector<Vector2>& getUVMap()  const { return m_UVMap;                   }

    void update(float delta);
    void render(Renderer& renderer) const;
  private:
    Vector2 m_size;
    Texture* m_texture;
    std::vector<Vector2> m_UVMap;
};

#endif // RENDERABLE_H
