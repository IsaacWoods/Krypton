#include <util/shader.hpp>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <util/util.hpp>

//#define SHADER_DEBUG

static std::string loadShader(const std::string& fileName);
static void checkShaderError(GLuint handle, int flag, bool isProgram, const std::string& errorMessage);
static void traverseShaderSource(GLuint programHandle, std::map<std::string, GLuint>& uniformMap, const std::string& shaderSource);

Shader::Shader(const std::string& name)
{
  m_programHandle = glCreateProgram();

  if (!m_programHandle)
  {
    std::cerr << "Could not create shader program: ";
    Util::checkGLError();
    Util::pauseConfirm();
  }

  generateShader(loadShader(name + ".vertex"), loadShader(name + ".fragment"));
}

Shader::~Shader()
{
  glDeleteProgram(m_programHandle);
}

void Shader::bind() const
{
  glUseProgram(m_programHandle);
}

void Shader::unbind() const
{
  glUseProgram(0);
}

void Shader::generateShader(const std::string& vertexSource, const std::string& fragmentSource)
{
  GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
  GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

  const char* cVertexSource = vertexSource.c_str();
  const char* cFragmentSource = fragmentSource.c_str();

  glShaderSource(vertexShader, 1, &cVertexSource, NULL);
  glCompileShader(vertexShader);
  checkShaderError(vertexShader, GL_COMPILE_STATUS, false, "Failed to compile vertex shader");

  glShaderSource(fragmentShader, 1, &cFragmentSource, NULL);
  glCompileShader(fragmentShader);
  checkShaderError(fragmentShader, GL_COMPILE_STATUS, false, "Failed to compile fragment shader");

  glAttachShader(m_programHandle, vertexShader);
  glAttachShader(m_programHandle, fragmentShader);

  glLinkProgram(m_programHandle);
  checkShaderError(m_programHandle, GL_LINK_STATUS, true, "Error linking shader program");

  glValidateProgram(m_programHandle);
  checkShaderError(m_programHandle, GL_VALIDATE_STATUS, true, "Error validating shader program");

  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);

  traverseShaderSource(m_programHandle, m_uniformMap, vertexSource);
  traverseShaderSource(m_programHandle, m_uniformMap, fragmentSource);
}

void Shader::streamUniform(const std::string& mapping, int value) const
{
  glUniform1i(m_uniformMap.at(mapping), value);
}

void Shader::streamUniform(const std::string& mapping, float value) const
{
  glUniform1f(m_uniformMap.at(mapping), value);
}

void Shader::streamUniform(const std::string& mapping, const Vector2& value) const
{
  glUniform2f(m_uniformMap.at(mapping), value.x(), value.y());
}

void Shader::streamUniform(const std::string& mapping, const Vector3& value) const
{
  //glUniform3f(m_uniformMap.at(mapping), value.x(), value.y(), value.z());
  glUniform3f(glGetUniformLocation(m_programHandle, mapping.c_str()), value.x(), value.y(), value.z());
}

void Shader::streamUniform(const std::string& mapping, const Vector4& value) const
{
  glUniform4f(m_uniformMap.at(mapping), value.x(), value.y(), value.z(), value.w());
}

void Shader::streamUniform(const std::string& mapping, Matrix4 value) const
{
  glUniformMatrix4fv(m_uniformMap.at(mapping), 1, GL_FALSE, &(value[0*0+0]));
}

void Shader::streamUniform(const std::string& mapping, unsigned int samplerSlot, const Texture& texture) const
{
  texture.bind(samplerSlot);
  glUniform1i(m_uniformMap.at(mapping), samplerSlot);
}

static void checkShaderError(GLuint handle, int flag, bool isProgram, const std::string& errorMessage)
{
  GLint success = 0, length;
  std::string logMessage;

  if (isProgram)
    glGetProgramiv(handle, flag, &success);
  else
    glGetShaderiv(handle, flag, &success);

  if (!success)
  {
    if (isProgram)
    {
      glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &length);
      std::vector<char> error(length);
      glGetProgramInfoLog(handle, length, &length, &error[0]);
      logMessage = std::string(error.data(), error.size());

      glDeleteProgram(handle);
    }
    else
    {
      glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &length);
      std::vector<char> error(length);
      glGetShaderInfoLog(handle, length, &length, &error[0]);
      logMessage = std::string(error.data(), error.size());

      glDeleteShader(handle);
    }

    fprintf(stderr, "%s: '%s'\n", errorMessage.c_str(), logMessage.c_str());
    Util::pauseConfirm();
  }
}

static std::string loadShader(const std::string& fileName)
{
  std::ifstream file;
  file.open(("assets/shaders/" + fileName).c_str());

  std::string output, line;

  if (file.is_open())
  {
    while (file.good())
    {
      getline(file, line);

      if (line.find("#include") == std::string::npos)
        output.append(line + "\n");
      else
      {
        std::string includeFileName = Util::split(line, ' ')[1];
        includeFileName = includeFileName.substr(1, includeFileName.length() - 2);

        std::string included = loadShader(includeFileName);
        output.append(included + "\n");
      }
    }
  }
  else
  {
    std::cerr << "Unable to load a shader from file: '" << fileName << "'!" << std::endl;
    Util::pauseConfirm();
  }

  return output;
}

/*
 * Do operations on a line of shader code to check for uniform declarations,
 * or anything else we might need to detect.
 * TODO: detect structs as uniforms, and correctly add their locations to the map
 */
static void traverseShaderSource(GLuint programHandle, std::map<std::string, GLuint>& uniformMap, const std::string& shaderSource)
{
  static const std::string UNIFORM_KEY = "uniform";

  // --- Check for uniform variable declaration ---
  size_t uniformLocation = shaderSource.find(UNIFORM_KEY);

  while (uniformLocation != std::string::npos)
  {
    bool isCommented = false;
    size_t lastLineEnd = shaderSource.rfind("\n", uniformLocation);

    if (lastLineEnd != std::string::npos)
    {
      std::string potentialCommentSection = shaderSource.substr(lastLineEnd, uniformLocation - lastLineEnd);
      isCommented = (potentialCommentSection.find("//") != std::string::npos);
    }

    if (!isCommented)
    {
      // TODO: support optionally adding a default value declaration, like 'uniform mat4 projectionMatrix = mat4(1.0);'
      size_t begin = uniformLocation + UNIFORM_KEY.length();
      size_t end = shaderSource.find(";", begin);

      std::string uniformLine = shaderSource.substr(begin + 1, end - begin - 1);
      begin = uniformLine.find(" ");
      std::string uniformName = uniformLine = uniformLine.substr(begin + 1);

      GLuint uniformBinding = glGetUniformLocation(programHandle, uniformName.c_str());
      assert(uniformBinding != GL_INVALID_VALUE && uniformBinding != GL_INVALID_OPERATION);

      uniformMap.insert(std::pair<std::string, unsigned int>(uniformName, uniformBinding));
      #ifdef SHADER_DEBUG
        std::cout << "Registed uniform variable: " << uniformName << std::endl;
      #endif
      uniformLocation = shaderSource.find(UNIFORM_KEY, uniformLocation + UNIFORM_KEY.length());
    }
  }
}
