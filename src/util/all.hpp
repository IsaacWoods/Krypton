#ifndef UTIL_ALL_HPP
#define UTIL_ALL_HPP

#include <util/LIB_stb_image.h>
#include <util/maths.hpp>
#include <util/shader.hpp>
#include <util/texture.hpp>
#include <util/util.hpp>
#include <util/window.hpp>

#endif // UTIL_ALL_HPP
