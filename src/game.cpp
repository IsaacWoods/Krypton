#include <game.hpp>

Game::Game(const KryptonOptions& options) :
	m_options(options),
	m_frameTime(1.0d / options.desiredFramerate),
	m_window(options),
	m_renderer(options)
{ }

void Game::start()
{
	std::cout << "--- Starting Krypton ---" << std::endl;
	this->init();

	double lastTime = Util::getTime();  // Current time at the start of the last frame
  double frameCounter = 0;            // Total passed time since last frame counter display (ms)
  double unprocessedTime = 0;         // Amount of passed time that we havn't accounted for (ms)
  int frames = 0;                     // Number of frames rendered since last iteration

  while (isRunning())
  {
    bool shouldRender = false;

    double startTime = Util::getTime();
    double passedTime = startTime - lastTime;
    lastTime = startTime;

    unprocessedTime += passedTime;
    frameCounter += passedTime;

    if (frameCounter >= m_options.profileFrequency && m_options.shouldProfile)
    {
      // TODO: do profiling
      double totalElapsedTime = ((1000.0d * frameCounter) / ((double) frames));

      std::cout << "Elapsed Time: " << totalElapsedTime << " ms" << std::endl;
      frames = 0;
      frameCounter = 0;
    }

    while (unprocessedTime > m_frameTime)
    {
      // TODO: poll the window for events, so we can extract input etc.

      this->update((float) m_frameTime);
      shouldRender = true;
      unprocessedTime -= m_frameTime;
    }

    if (shouldRender)
    {
      this->render(m_renderer);
      m_window.update();
      frames++;
    }
    else
      Util::sleep(1);
  }

  std::cout << "--- Stopping Krypton ---" << std::endl;
  cleanUp();
}

bool Game::isRunning() const
{
	return !m_window.shouldClose();
}

void Game::cleanUp()
{
	m_window.destroy();
}
