#ifndef MATHS_H
#define MATHS_H

#include <math.h>
#include <tuple>
#include <string>

#define PI 3.1415926535897932384626433832795
#define toRadians(x) (float)(x * PI / 180.0f)
#define toDegrees(x) (float)(x * 180.0f / PI)

template<unsigned int D>
struct Vector
{
  float values[D];

  Vector()
  {
    for (unsigned int i = 0; i < D; i++)
      values[i] = 0;
  }

  Vector(const Vector<D>& v)
  {
    for (unsigned int i = 0; i < D; i++)
      values[i] = v[i];
  }

  /*template<typename... Args>
  inline void setTo(Args&&... values)
  {
    auto tuple = std::make_tuple(std::forward<Args>(values)...);
    unsigned int i = 0;

    for (auto value : tuple)
      values[i++] = value;
  }*/

  inline float dot(const Vector<D>& v) const
  {
    float result;

    for (unsigned int i = 0; i < D; i++)
      result += values[i] * v[i];

    return result;
  }

  inline float lengthSquared() const
  {
    return this->dot(*this);
  }

  inline float length() const
  {
    return sqrt(lengthSquared());
  }

  inline Vector<D> normalised() const
  {
    return *this / length;
  }

  inline Vector<D> lerp(const Vector<D>& v, float lerpFactor) const
  {
    return (v - *this) * lerpFactor + *this;
  }

  inline Vector<D> reflect(const Vector<D>& normal) const
  {
    return *this - (normal * (this->dot(normal) * 2));
  }

  inline Vector<D> operator+(const Vector<D>& v) const
  {
    Vector<D> result;

    for (unsigned int i = 0; i < D; i++)
      result[i] = values[i] + v[i];

    return result;
  }

  inline Vector<D> operator-(const Vector<D>& v) const
  {
    Vector<D> result;

    for (unsigned int i = 0; i < D; i++)
      result[i] = values[i] - v[i];

    return result;
  }

  inline Vector<D> operator*(const Vector<D>& v) const
  {
    Vector<D> result;

    for (unsigned int i = 0; i < D; i++)
      result[i] = values[i] * v[i];

    return result;
  }

  inline Vector<D> operator*(float v) const
  {
    Vector<D> result;

    for (unsigned int i = 0; i < D; i++)
      result[i] = values[i] * v;

    return result;
  }

  inline Vector<D> operator/(const Vector<D>& v) const
  {
    Vector<D> result;

    for (unsigned int i = 0; i < D; i++)
      result[i] = values[i] / v[i];

    return result;
  }

  inline Vector<D> operator/(float v) const
  {
    Vector<D> result;

    for (unsigned int i = 0; i < D; i++)
      result[i] = values[i] / v;

    return result;
  }

  inline Vector<D>& operator+=(const Vector<D>& v)
  {
    for (unsigned int i = 0; i < D; i++)
      values[i] = values[i] + v[i];

    return *this;
  }

  inline Vector<D>& operator-=(const Vector<D>& v)
  {
    for (unsigned int i = 0; i < D; i++)
      values[i] = values[i] - v[i];

    return *this;
  }

  inline Vector<D>& operator*=(const Vector<D>& v)
  {
    for (unsigned int i = 0; i < D; i++)
      values[i] = values[i] * v[i];

    return *this;
  }

  inline Vector<D>& operator/=(const Vector<D>& v)
  {
    for (unsigned int i = 0; i < D; i++)
      values[i] = values[i] / v[i];

    return *this;
  }

  float& operator[](unsigned int i)
  {
    return values[i];
  }

  float operator[](unsigned int i) const
  {
    return values[i];
  }

  inline bool operator==(const Vector<D>& v) const
  {
    for (unsigned int i = 0; i < D; i++)
      if (values[i] != v[i])
        return false;

    return true;
  }

  inline bool operator!=(const Vector<D>& v) const
  {
    return !operator==(v);
  }
};

struct Vector2 : public Vector<2>
{
  Vector2(float x = 0, float y = 0) : Vector<2>()
  {
    values[0] = x;
    values[1] = y;
  }

  Vector2(const Vector<2>& base) : Vector<2>(base) { }

  inline float x()  const { return values[0]; }
  inline float y()  const { return values[1]; }

  inline void x(float x)  { values[0] = x;   }
  inline void y(float y)  { values[1] = y;   }

  inline void setTo(float x, float y)
  {
    values[0] = x;
    values[1] = y;
  }

  inline std::string dump() const
  {
    return "[" + std::to_string(values[0]) + ", " + std::to_string(values[1]) + "]";
  }
};

struct Vector3 : public Vector<3>
{
  Vector3(float x = 0, float y = 0, float z = 0) : Vector<3>()
  {
    values[0] = x;
    values[1] = y;
    values[2] = z;
  }

  Vector3(const Vector<3>& base) : Vector<3>(base) { }

  inline float x()  const { return values[0]; }
  inline float y()  const { return values[1]; }
  inline float z()  const { return values[2]; }

  inline void x(float x)  { values[0] = x;   }
  inline void y(float y)  { values[1] = y;   }
  inline void z(float z)  { values[2] = z;   }

  inline void setTo(float x, float y, float z)
  {
    values[0] = x;
    values[1] = y;
    values[2] = z;
  }

  inline std::string dump() const
  {
    return "[" + std::to_string(values[0]) + ", " + std::to_string(values[1]) + ", " + std::to_string(values[2]) + "]";
  }
};

struct Vector4 : public Vector<4>
{
    Vector4(float x = 0, float y = 0, float z = 0, float w = 0) : Vector<4>()
    {
      values[0] = x;
      values[1] = y;
      values[2] = z;
      values[3] = w;
    }

    Vector4(const Vector<4>& base) : Vector<4>(base) { }

    inline float x()  const { return values[0]; }
    inline float y()  const { return values[1]; }
    inline float z()  const { return values[2]; }
    inline float w()  const { return values[3]; }

    inline void x(float x)  { values[0] = x;   }
    inline void y(float y)  { values[1] = y;   }
    inline void z(float z)  { values[2] = z;   }
    inline void w(float w)  { values[3] = w;   }

    inline void setTo(float x, float y, float z, float w)
    {
      values[0] = x;
      values[1] = y;
      values[2] = z;
      values[3] = w;
    }

    inline std::string dump() const
    {
      return "[" + std::to_string(values[0]) + ", " + std::to_string(values[1]) + ", " + std::to_string(values[2]) + ", " + std::to_string(values[3]) + "]";
    }
};

struct Matrix4
{
  float m[4 * 4];

  Matrix4()
  {
    m[0*4+0] = 1;   m[0*4+1] = 0;   m[0*4+2] = 0;   m[0*4+3] = 0;
    m[1*4+0] = 0;   m[1*4+1] = 1;   m[1*4+2] = 0;   m[1*4+3] = 0;
    m[2*4+0] = 0;   m[2*4+1] = 0;   m[2*4+2] = 1;   m[2*4+3] = 0;
    m[3*4+0] = 0;   m[3*4+1] = 0;   m[3*4+2] = 0;   m[3*4+3] = 1;
  }

  inline Matrix4 operator*(const Matrix4& mat) const
  {
    Matrix4 result;

    for (unsigned int i = 0; i < 4; i++)
      for (unsigned int j = 0; j < 4; j++)
      {
        result.m[i*4+j] = 0;

        for (unsigned int k = 0; k < 4; k++)
          result.m[i*4+j] += m[k*4+j] * mat.m[i*4+k];
      }

    return result;
  }

  inline float& operator[](unsigned int i)
  {
    return m[i];
  }

  inline float operator[](unsigned int i) const
  {
    return m[i];
  }

  static Matrix4 orthographicProj(float left, float right, float bottom, float top, float near, float far)
  {
    Matrix4 r;

    const float width = (right - left);
    const float height = (top - bottom);
    const float depth = (far - near);

    r[0+0*4] = 2.0f / width;
		r[1+1*4] = 2.0f / height;
		r[2+2*4] = 2.0f / depth;

    r[0+3*4] = -(left + right) / width;
		r[1+3*4] = -(bottom + top) / height;
		r[2+3*4] = -(far + near) / depth;

    return r;
  }

  static Matrix4 translation(const Vector2& translation)
  {
    Matrix4 r;

    r.m[0*4+0] = 1;   r.m[0*4+1] = 0;   r.m[0*4+2] = 0;   r.m[0*4+3] = translation.x();
    r.m[1*4+0] = 0;   r.m[1*4+1] = 1;   r.m[1*4+2] = 0;   r.m[1*4+3] = translation.y();
    r.m[2*4+0] = 0;   r.m[2*4+1] = 0;   r.m[2*4+2] = 1;   r.m[2*4+3] = 0;
    r.m[3*4+0] = 0;   r.m[3*4+1] = 0;   r.m[3*4+2] = 0;   r.m[3*4+3] = 1;

    return r;
  }

  static Matrix4 rotation(float angle)
  {
    Matrix4 r;

    r.m[0*4+0] = cos(angle);    r.m[0*4+1] = -sin(angle);     r.m[0*4+2] = 0;   r.m[0*4+3] = 0;
    r.m[1*4+0] = sin(angle);    r.m[1*4+1] = cos(angle);    r.m[1*4+2] = 0;   r.m[1*4+3] = 0;
    r.m[2*4+0] = 0;         r.m[2*4+1] = 0;         r.m[2*4+2] = 1;   r.m[2*4+3] = 0;
    r.m[3*4+0] = 0;         r.m[3*4+1] = 0;         r.m[3*4+2] = 0;   r.m[3*4+3] = 1;

    return r;
  }

  static Matrix4 scale(const Vector2& scale)
  {
    Matrix4 r;

    r.m[0*4+0] = scale.x();   r.m[0*4+1] = 0;       r.m[0*4+2] = 0;   r.m[0*4+3] = 0;
    r.m[1*4+0] = 0;       r.m[1*4+1] = scale.y();   r.m[1*4+2] = 0;   r.m[1*4+3] = 0;
    r.m[2*4+0] = 0;       r.m[2*4+1] = 0;       r.m[2*4+2] = 1;   r.m[2*4+3] = 0;
    r.m[3*4+0] = 0;       r.m[3*4+1] = 0;       r.m[3*4+2] = 0;   r.m[3*4+3] = 1;

    return r;
  }
};

struct Vertex
{
  Vector2 position;
  Vector2 uv;

  Vertex(const Vector2& position, const Vector2& uv) :
    position(position),
    uv(uv)
  { }
};

#endif // MATHS_H
