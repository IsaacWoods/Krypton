#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <util/maths.hpp>
#include <util/texture.hpp>
#include <scene/gameobject.hpp>

class Scene
{
  public:
    Scene(unsigned int width, unsigned int height) :
      m_width(width),
      m_height(height),
      m_scale(1.0f, 1.0f)
    {
      updateProjectionMatrix();
    }

    virtual ~Scene();

    Scene& addObject(GameObject* object);
    inline int getWidth()                           const { return m_width;              }
    inline int getHeight()                          const { return m_height;             }
    inline std::vector<GameObject*>& getObjects()         { return m_objects;            }
    inline std::vector<GameObject*> getObjects()    const { return m_objects;            }
    inline Matrix4 getProjectionMatrix()            const { return m_projectionMatrix;   }
    inline const Vector2& getScale()                const { return m_scale;              }

    void setSize(int width, int height);
    inline void setScale(const Vector2& scale)            { m_scale = scale;             }

    virtual void update(float delta);
    virtual void render(Renderer& renderer) const;
  protected:
    unsigned int m_width, m_height;
    Matrix4 m_projectionMatrix;
    Vector2 m_scale;
    std::vector<GameObject*> m_objects;

    void updateProjectionMatrix();
};

#endif // SCENE_H
