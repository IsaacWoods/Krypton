#ifndef TILEDSCENE_H
#define TILEDSCENE_H

#include <vector>
#include <scene/all.hpp>
#include <util/texture.hpp>
#include <util/maths.hpp>

struct TileDefinition
{
  TileDefinition(int id, const Texture* texture, const std::vector<Vector2>& uvMap) :
    id(id),
    texture(texture),
    uvMap(uvMap)
  { }

  int id;
  const Texture* texture;
  std::vector<Vector2> uvMap;
};

struct Tile
{
  Tile() = default;

  Tile(unsigned short xPos, unsigned short yPos, int definitionID) :
    xPos(xPos),
    yPos(yPos),
    definitionID(definitionID)
  { }

  unsigned short xPos;
  unsigned short yPos;
  int definitionID;
};

class TiledScene : public Scene
{
  public:
    TiledScene(unsigned int tileWidth, unsigned int tileHeight, unsigned int tileSize);
    ~TiledScene();

    int addTileDefinition(TileDefinition* definition);
    void setTile(unsigned short xPos, unsigned short yPos, int tileDef);
    inline unsigned int getTileSize()     const { return m_tileSize;      }
    inline Vector2 getTileSizeVec()       const { return m_tileSizeVec;   }

    void update(float delta);
    void render(Renderer& renderer) const;
  private:
    unsigned int m_tileWidth, m_tileHeight;
    unsigned int m_tileSize;
    Vector2 m_tileSizeVec;
    std::vector<TileDefinition*> m_tileDefinitions;
    int m_currentDefinition;
    Tile* m_tiles;
};

#endif // TILEDSCENE_H
