#ifndef LIGHT_H
#define LIGHT_H

#include <util/maths.hpp>

struct Attenuation
{
	Attenuation(float constant, float linear, float exponent) :
		constant(constant),
		linear(linear),
		exponent(exponent)
	{ }

	float constant;
	float linear;
	float exponent;
};

class Light
{
	public:
		Light(const Vector2& position, const Vector3& color, float intensity, const Attenuation& attenuation) :
			m_position(position),
			m_color(color),
			m_intensity(intensity),
			m_attenuation(attenuation)
		{ }

		inline const Vector2& getPosition()          const { return m_position;    }
		inline const Vector3& getColor()             const { return m_color;       }
		inline float getIntensity()                  const { return m_intensity;   }
		inline const Attenuation& getAttenuation()   const { return m_attenuation; }
	protected:
		Vector2 m_position;
		Vector3 m_color;
		float m_intensity;
		Attenuation m_attenuation;
};

#endif // LIGHT_H
