# Krypton
Krypton is a super-fast 2D Batch Renderer I wrote at 14 as a side project. It boasts a component-based scene hierarchy, acceleration structure support and a 2D batch-renderer with support
for full 2D lighting, shadowing and a bunch of graphical effects.

### Build Dependencies
* C++11 supporting compiler
* OpenGL 3.3
* GLFW3
* GLEW

### Current Todo
* Allow the batch-renderer's shader to take an array of textures
* Format LIB_stb_image.hpp to conform with the standards - remove excess code and format to look all pretty
* Alter so that level is independent of screen scaling
* Make tiles at high-resolutions match up at edges when the window is too large
* Lighting framework
* Shadow mapping implementation
* Write level serialisation format

### Additional Credit
* [BennyQBD](https://github.com/BennyQBD) - inspired the maths library and some of the shader framework
* [TheCherno](https://github.com/TheCherno) - inspired the base for the batch rendering system
* [Contributers to the STBImage library](https://github.com/nothings/stb) - great library, adapted into its current form within Krypton
