#include <game.hpp>

class Test : public Game
{
	public:
		Test(const KryptonOptions& options);

		void init();
		void update(float delta);
		void render(Renderer& renderer);
	private:
		Scene m_scene;
};

Test::Test(const KryptonOptions& options) :
	Game(options),
	m_scene(1000, 800)
{ }

void Test::init()
{
	GameObject* myObject = new GameObject(Vector2(200, 200), toRadians(45.0f));
	GameObject* myObject2 = new GameObject(Vector2(800, 600));
	Texture* texture = new Texture("test.png");
	m_scene.addObject(myObject);
	m_scene.addObject(myObject2);

	myObject->addComponent(new Renderable(Vector2(200, 400), texture));
	myObject2->addComponent(new Renderable(Vector2(100, 100), texture));
}

void Test::update(float delta)
{
	m_scene.update(delta);
}

void Test::render(Renderer& renderer)
{
	m_scene.render(renderer);
}

int main()
{
  KryptonOptions options;
  options.width = 1000;
  options.height = 800;

  Test game(options);
  game.start();

  return 0;
}
