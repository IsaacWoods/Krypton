#include <GL/glew.h>
#include <util/window.hpp>
#include <util/util.hpp>

Window::Window(const KryptonOptions& options) :
  m_options(options)
{
  if (!glfwInit())
  {
    std::cerr << "FATAL: We couldn't create a GLFW context!" << std::endl;
    exit(666);
  }

  glfwSetErrorCallback(errorCallback);
  // glfwSetFramebufferSizeCallback(m_window, framebufferSizeCallback);
  m_window = glfwCreateWindow(options.width, options.height, options.windowTitle.c_str(), NULL, NULL);

  if (!m_window)
  {
    glfwTerminate();

    std::cerr << "FATAL: We couldn't create a GLFW window handle!" << std::endl;
    Util::pauseConfirm();
  }

  glfwMakeContextCurrent(m_window);
  glewInit();
}

Window::~Window()
{
  destroy();
}

void Window::destroy()
{
  glfwDestroyWindow(m_window);
  glfwTerminate();
}

void Window::errorCallback(int error, const char* description)
{
  std::cerr << "GLFW error reported (" << error << ") - " << description << std::endl;
}

void Window::framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
  glViewport(0, 0, width, height);
}

bool Window::shouldClose() const
{
  return glfwWindowShouldClose(m_window);
}

void Window::update()
{
  glfwSwapBuffers(m_window);
  glfwPollEvents();
}
