#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <GL/glew.h>

class Texture
{
  public:
    Texture(const std::string& name, GLenum target = GL_TEXTURE_2D, GLfloat filter = GL_NEAREST_MIPMAP_LINEAR, GLenum internalFormat = GL_RGBA, GLenum format = GL_RGBA, bool clamp = true, GLenum attachment = GL_NONE);
    Texture(unsigned int width, unsigned int height, unsigned char* data, GLenum target = GL_TEXTURE_2D, GLfloat filter = GL_NEAREST_MIPMAP_LINEAR, GLenum internalFormat = GL_RGBA, GLenum format = GL_RGBA, bool clamp = true, GLenum attachment = GL_NONE);
    Texture(const Texture& texture);
    void operator=(Texture texture);
    ~Texture();

    void bind(unsigned int texSlot = 0) const;
    void bindAsRenderTarget() const;
    void unbind() const;

    inline unsigned int getWidth()  const { return m_width;   }
    inline unsigned int getHeight() const { return m_height;  }
  private:
    int m_width, m_height;
    GLuint m_handle;
    GLenum m_target;
    GLuint m_frameBuffer, m_renderBuffer;

    void generateTexture(unsigned char* data, GLfloat filter, GLenum interalFormat, GLenum format, bool clamp);
    void generateRenderTarget(GLenum attachment);
};

#endif // TEXTURE_H
