#ifndef GAME_H
#define GAME_H

#include <GL/glew.h>
#include <rendering/all.hpp>
#include <scene/all.hpp>
#include <util/all.hpp>

class Game
{
  public:
    Game(const KryptonOptions& options);
    ~Game() { }

    virtual void init() = 0;
    virtual void update(float delta) = 0;
    virtual void render(Renderer& renderer) = 0;

    void start();
    bool isRunning() const;
  private:
    void cleanUp();

    KryptonOptions m_options;
    const double m_frameTime;
    Window m_window;
    Renderer m_renderer;
};

#endif // GAME_H
