#include <scene/tiledscene.hpp>
#include <rendering/renderer.hpp>

TiledScene::TiledScene(unsigned int tileWidth, unsigned int tileHeight, unsigned int tileSize) :
  Scene(tileWidth * tileSize, tileHeight * tileSize),
  m_tileWidth(tileWidth),
  m_tileHeight(tileHeight),
  m_tileSize(tileSize),
  m_tileSizeVec(tileSize, tileSize),
  m_currentDefinition(0)
{
  m_tiles = new Tile[tileWidth * tileHeight];

  for (unsigned int y = 0; y < tileHeight; y++)
    for (unsigned int x = 0; x < tileWidth; x++)
      m_tiles[x + y * tileWidth] = Tile(x, y, -1);
}

TiledScene::~TiledScene()
{
  for (auto* definition : m_tileDefinitions)
    delete definition;

  delete[] m_tiles;
}

int TiledScene::addTileDefinition(TileDefinition* definition)
{
  m_tileDefinitions.push_back(definition);
  return m_currentDefinition++;
}

void TiledScene::setTile(unsigned short xPos, unsigned short yPos, int tileDef)
{
  m_tiles[xPos + yPos * m_tileWidth].definitionID = tileDef;
}

void TiledScene::update(float delta)
{
  Scene::update(delta);
}

void TiledScene::render(Renderer& renderer) const
{
  renderer.prepareForScene();
  renderer.startBatch(m_projectionMatrix, m_scale);
  Vector2 positionStore;

  renderer.addTexture(*(m_tileDefinitions[0]->texture));

  for (unsigned int i = 0; i < m_tileWidth * m_tileHeight; i++)
  {
    if (m_tiles[i].definitionID != -1)
    {
      positionStore.setTo(m_tiles[i].xPos * m_tileSize, m_tiles[i].yPos * m_tileSize);
      renderer.submit(positionStore, m_tileSizeVec, m_tileDefinitions[m_tiles[i].definitionID]->uvMap);
    }
  }

  renderer.flushBatch();
  renderer.startBatch(m_projectionMatrix, m_scale);

  for (auto* object : m_objects)
    object->render(renderer);

  renderer.flushBatch();
}
