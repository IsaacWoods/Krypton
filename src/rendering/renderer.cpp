#include <rendering/renderer.hpp>
#include <iostream>
#include <rendering/renderable.hpp>
#include <scene/scene.hpp>

#define POSITION_INDEX  0
#define UV_INDEX        1

Renderer::Renderer(const KryptonOptions& options) :
  m_options(options),
  m_isBatching(false),
  m_shader(options.shaderName)
{
  glGenVertexArrays(1, &m_vertexArrayHandle);
  glGenBuffers(1, &m_vertexBufferHandle);

  glBindVertexArray(m_vertexArrayHandle);
  glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferHandle);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * options.maxBatchingSize * 4, NULL, GL_DYNAMIC_DRAW);

  glEnableVertexAttribArray(POSITION_INDEX);
  glEnableVertexAttribArray(UV_INDEX);

  glVertexAttribPointer(POSITION_INDEX, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*) 0);
  glVertexAttribPointer(UV_INDEX, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*) offsetof(Vertex, uv));

  GLuint* indices = new GLuint[options.maxBatchingSize * 6];
  int offset = 0;

  for (unsigned int i = 0; i < options.maxBatchingSize * 6; i += ((offset += 4), 6))    // Add 4 to 'offset', add 6 to 'i' - Simples!
  {
    indices[  i  ] = offset + 0;
    indices[i + 1] = offset + 1;
    indices[i + 2] = offset + 2;

    indices[i + 3] = offset + 2;
    indices[i + 4] = offset + 3;
    indices[i + 5] = offset + 0;
  }

  glGenBuffers(1, &m_indexBufferHandle);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBufferHandle);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * options.maxBatchingSize * 6, indices, GL_STATIC_DRAW);

  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  delete[] indices;
}

Renderer::~Renderer()
{ }

void Renderer::prepareForScene()
{
  if (m_isBatching)
  {
    std::cerr << "Tried to prepare for a batch when the renderer was already batching!" << std::endl;
    Util::pauseConfirm();
  }

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::startBatch(const Matrix4& projectionMatrix, const Vector2& sceneScale)
{
  if (m_isBatching)
  {
    std::cerr << "Tried to start a batch when the renderer was already batching!" << std::endl;
    Util::pauseConfirm();
  }

  // --- Set current batch's stuff and things ---
  m_batchingProjectionMatrix = &projectionMatrix;
  m_batchingSceneScale = &sceneScale;
  m_isBatching = true;

  m_indexCount = 0;
  glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferHandle);
  m_buffer = (Vertex*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

  m_shader.bind();

  if (!m_buffer)
  {
    std::cerr << "Couldn't map buffer: ";
    Util::checkGLError();
    Util::pauseConfirm();
  }

  #ifdef CHECK_BATCH_OVERFLOW
    m_batchCounter = 0;
  #endif
}

void Renderer::submit(const Vector2& position, const Vector2& size)
{
  #ifdef CHECK_BATCH_OVERFLOW
    if (m_batchCounter >= m_options.maxBatchingSize)
    {
      std::cerr << "Batch Overflow occured!" << std::endl;
      Util::pauseConfirm();
    }
  #endif

  m_buffer->position = position;
  m_buffer->uv = Vector2(0, 1);
  m_buffer++;

  m_buffer->position = Vector2(position.x(), position.y() + size.y());
  m_buffer->uv = Vector2(0, 0);
  m_buffer++;

  m_buffer->position = Vector2(position.x() + size.x(), position.y() + size.y());
  m_buffer->uv = Vector2(1, 0);
  m_buffer++;

  m_buffer->position = Vector2(position.x() + size.x(), position.y());
  m_buffer->uv = Vector2(1, 1);
  m_buffer++;

  m_indexCount += 6;

  #ifdef CHECK_BATCH_OVERFLOW
    m_batchCounter++;
  #endif
}

void Renderer::submit(const Vector2& position, const Vector2& size, const std::vector<Vector2>& uvMap)
{
  #ifdef CHECK_BATCH_OVERFLOW
    if (m_batchCounter >= m_options.maxBatchingSize)
    {
      std::cerr << "Batch Overflow occured!" << std::endl;
      Util::pauseConfirm();
    }
  #endif

  m_buffer->position = position;
  m_buffer->uv = uvMap[0];
  m_buffer++;

  m_buffer->position = Vector2(position.x(), position.y() + size.y());
  m_buffer->uv = uvMap[1];
  m_buffer++;

  m_buffer->position = Vector2(position.x() + size.x(), position.y() + size.y());
  m_buffer->uv = uvMap[2];
  m_buffer++;

  m_buffer->position = Vector2(position.x() + size.x(), position.y());
  m_buffer->uv = uvMap[3];
  m_buffer++;

  m_indexCount += 6;

  #ifdef CHECK_BATCH_OVERFLOW
    m_batchCounter++;
  #endif
}

// TODO: submit an array of positions, sizes, and uvMaps and just copy it into the mapped buffer directly

void Renderer::flushBatch()
{
  if (!m_isBatching)
  {
    std::cerr << "Tried to flush a batch when the renderer wasn't batching!" << std::endl;
    Util::pauseConfirm();
  }

  glBindVertexArray(m_vertexArrayHandle);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBufferHandle);

  // for (unsigned int i = 0; i < m_lights.size(); i++)
  // {
  //   const Light* light = m_lights[i];
  //   std::string uniformBase = "lights[" + i + std::string("].");
  //   m_shader.streamUniform(uniformBase + "position", light->getPosition());
  //   m_shader.streamUniform(uniformBase + "color", light->getColor());
  //   m_shader.streamUniform(uniformBase + "intensity", light->getIntensity());
  //   m_shader.streamUniform(uniformBase + "attenuation.constant", light->getAttenuation().constant);
  //   m_shader.streamUniform(uniformBase + "attenuation.linear", light->getAttenuation().linear);
  //   m_shader.streamUniform(uniformBase + "attenuation.exponent", light->getAttenuation().exponent);
  // }

  m_shader.streamUniform("light.color", Vector3(1, 0, 1));

  m_shader.streamUniform("projectionMatrix", *m_batchingProjectionMatrix);
  m_shader.streamUniform("diffuseTexture", 0, *m_batchingDiffuseTexture);
  m_shader.streamUniform("sceneScale", *m_batchingSceneScale);

  glDepthMask(GL_FALSE);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glUnmapBuffer(GL_ARRAY_BUFFER);
  glDrawElements(GL_TRIANGLES, m_indexCount, GL_UNSIGNED_INT, NULL);
  glDisable(GL_BLEND);
  glDepthMask(GL_TRUE);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
  m_isBatching = false;
  m_indexCount = 0;
  m_shader.unbind();
}
