#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <vector>
#include <util/maths.hpp>

class Component;
class Renderer;

class GameObject
{
  public:
    GameObject(const Vector2& position = Vector2(0.0f, 0.0f), float rotation = 0.0f);
    ~GameObject();

    inline Vector2& getPosition()                                 { return m_position;     }
    inline const Vector2& getPosition()                     const { return m_position;     }
    inline float& getRotation()                                   { return m_rotation;     }
    inline float getRotation()                              const { return m_rotation;     }
    inline std::vector<Component*>& getComponents()               { return m_components;   }
    inline const std::vector<Component*>& getComponents()   const { return m_components;   }

    GameObject& addComponent(Component* component);
    void update(float delta);
    void render(Renderer& renderer) const;
  private:
    Vector2 m_position;
    float m_rotation;
    std::vector<Component*> m_components;
};

class Component
{
  public:
    Component() { }
    virtual ~Component() { }

    virtual void update(float delta) = 0;
    virtual void render(Renderer& renderer) const = 0;
    inline void setParent(GameObject* parent)  { m_parent = parent;  }
    inline const GameObject* getParent() const { return m_parent;    }
  protected:
    GameObject* m_parent;
};

#endif // GAMEOBJECT_H
