SRCS := $(shell find src -name '*.cpp')
OBJS := $(addprefix build/, $(SRCS:%.cpp=%.o))
CC = g++
CFLAGS = -Wall -std=c++0x -Isrc/ -I/usr/lib -O2
LFLAGS = -Wall -std=c++0x -Wl,--start-group -lglfw3 -lX11 -lXi -lXxf86vm -lpthread -lXrandr -lXinerama -lXcursor -lrt -lm -lGL -lGLU -lGLEW

Krypton: $(OBJS)
	$(CC) -o $@ $^ $(LFLAGS)

build/src/%.o : src/%.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean
clean:
	rm -rf build/*
	mkdir build/src
	mkdir build/src/rendering
	mkdir build/src/scene
	mkdir build/src/util
